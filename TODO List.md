# TODO List

[x] Create Docker environment
```sh
docker-compose up --build -d
```
[TC365 Test](http://tc365.ett.loc/)


[x] PHPMyAdmin
[PHPMyAdmin](http://tc365.ett.loc/phpmyadmin/index.php)


*Create Entity*

```sh
php bin/console make:entity
```

[ ] Author
[ ] Book

*Controllers*

```sh
php bin/console make:controller
```

[ ] DefaultController
[ ] AuthorController
[ ] BookController

*Forms*

```sh
php bin/console make:form
```

[ ] AuthorForm
[ ] BookForm
[ ] DeleteForm
[ ] SearchForm

*Views*
[ ] Base
[ ] Author list
[ ] Book list
[ ] Default (index, queries)
[ ] Create
[ ] Edit
[ ] Delete

*Repository*
[ ] BookRepository

*Service*
[ ] FileUploader

*EventListener*
[ ] FileManagerSubscriber

*DataFixtures*
[ ] author
[ ] book



