build:
	docker-compose up --build -d

up:
	docker-compose up -d

stop:
	docker stop $(docker ps -a -q) && docker system prune -af --volumes

web:
	docker exec -it tc365_web bash

mysql:
	docker exec -it tc365_sql bash

clearcache:
	app/bin/console cache:clear

installassets:
	app/bin/console assets:install

createschema:
	app/bin/console doctrine:schema:create	
