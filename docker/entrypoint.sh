#!/usr/bin/env bash

# -n Do not ask any interactive question
composer install -n
# bin/console doc:mig:mig --no-interaction
# bin/console doc:fix:load --no-interaction

php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:fixtures:load --no-interaction
php bin/console sonata:user:create --no-interaction --super-admin demo demo@demo.com demo

exec "$@"
