<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BookRepository;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/queries", name="queries")
     */
    public function queryAction(BookRepository $bookRepository)
    {
        $rep = $bookRepository;

        $sql = $rep->nativeSQLQuery();
        $doctrine = $rep->DoctrineQuery();
        
        return $this->render('default/queries.html.twig', [
            'sql' => $sql,
            'doctrine' => $doctrine,
        ]);
    }

}
