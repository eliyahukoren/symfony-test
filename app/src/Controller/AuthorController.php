<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Form\DeleteType;
use App\Form\FilterSingleType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    private $authorRepository;
    private $entityManager;


    public function __construct(
        AuthorRepository $authorRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->authorRepository = $authorRepository;
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/authors", name="author_list")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(FilterSingleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $data = $form->getData();
            $authors = $this->authorRepository->findAllByAnyField($data);
            
        } else {

            $authors = $this->authorRepository->findAll();
            
        }

        return $this->render('author/list.html.twig', [
            'authors' => $authors,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/author/create", name="author_create")
     */
    public function createAction(Request $request)
    {
        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $this->entityManager->persist($author);
            $this->entityManager->flush();
            
            return $this->redirectToRoute('author_list');
        }

        return $this->render('default/create.html.twig', array(
            'form' => $form->createView(),
            'class_object' => 'Create Author'
        ));
    }

    /**
     * @Route("/author/delete/{id}", name="author_delete")
     */
    public function deleteAction($id, Request $request){

        $author = $this->authorRepository->find($id);

        if( !$author ){
            throw $this->createNotFoundException(
                'Author with id: ' . $id . ' was not found.'
            );
        }

        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) { 
            if ($form->get('delete')->isClicked()){
                
                $this->entityManager->remove($author);
                $this->entityManager->flush();
            }

            return $this->redirectToRoute('author_list');
        }

        $classMessage = "Are you sure you want to delete " . $author->getName() . " ?";

        return $this->render('default/delete.html.twig', array(
            'form' => $form->createView(),
            'class_object' => $classMessage
        ));
    }

    /**
     * @Route("/author/update/{id}", name="author_update")
     */
    public function updateAction($id, Request $request)
    {
        $author = $this->authorRepository->find($id);

        if (!$author) {
            throw $this->createNotFoundException(
                'Author with id: ' . $id . ' was not found.'
            );
        }

        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $this->entityManager->flush();

            return $this->redirectToRoute('author_list');
        }

        return $this->render('default/edit.html.twig', array(
            'form' => $form->createView(),
            'class_object' => 'Edit ' . $author->getName()
        ));

    }


}
