<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Form\FilterSingleType;
use App\Form\DeleteType;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;



class BookController extends AbstractController
{
    private $bookRepository;
    private $entityManager;
    private $slugger;


    public function __construct(
        BookRepository $bookRepository,
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger
    ){
        $this->bookRepository = $bookRepository;
        $this->entityManager = $entityManager;
        $this->slugger = $slugger;
    }
    
    
    /**
     * @Route("/books", name="book_list")
     */
    public function index(Request $request): Response{
        
        $form = $this->createForm(FilterSingleType::class);

        $form->handleRequest($request);

        if( $form->isSubmitted() ){

            $data = $form->getData();
            $books = $this->bookRepository->findAllByAnyField($data);

        }else{

            $books = $this->bookRepository->findAll();

        }

        return $this->render('book/list.html.twig', [
            'books' => $books,
            'image_dir' => $this->getParameter('images_directory'),
            'form' => $form->createView()
        ]);
    }

    
    /**
     * @Route("/book/create", name="book_create")
     */
    public function createAction(Request $request){
        $book = new Book();

        $form = $this->createForm(BookType::class, $book);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $book->setImage(
                $this->manageImages($form)
            );
            $this->entityManager->persist($book);
            $this->entityManager->flush();

            return $this->redirectToRoute('book_list');
        }

        return $this->render('default/create.html.twig', array(
            'form' => $form->createView(),
            'class_object' => 'Book'
        ));
    }

    
    /**
     * @Route("/book/update/{id}", name="book_update")
     */
    public function updateAction($id, Request $request){
        $book = $this->bookRepository->find($id);
        
        if (!$book) {
            throw $this->createNotFoundException(
                'Book with id: ' . $id . ' was not found.'
            );
        }

        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $book->setImage(
                $this->manageImages($form)
            );

            $this->entityManager->flush();

            return $this->redirectToRoute('book_list');
        }

        return $this->render('default/edit.html.twig', array(
            'form' => $form->createView(),
            'class_object' => 'Book'
        ));
    }

    /**
     * @Route("/book/ajax", name="book_ajax")
     */
    public function ajaxAction(Request $request) :JsonResponse{

        $data = json_decode($request->getContent(), true);
        $book = $this->bookRepository->find($data['id']);

        
        $book->setName($data['name']);
        $book->setDescription($data['description']);
        $book->setYear($data['year']);

        $this->entityManager->flush();

        return new JsonResponse(array('status' => 'ok'));
    }

    
    /**
     * @Route("/book/delete/{id}", name="book_delete")
     */
    public function removeAction($id, Request $request){
        $book = $this->bookRepository->find($id);

        if (!$book) {
            throw $this->createNotFoundException(
                'Book with id: ' . $id . ' was not found.'
            );
        }

        $form = $this->createForm(DeleteType::class, $book);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('delete')->isClicked()) {

                $dir = $this->getParameter('images_directory');
                $filename = $dir . $book->getImage();

                $filesystem = new Filesystem();
                $filesystem->remove($filename);


                $this->entityManager->remove($book);
                $this->entityManager->flush();
            }

            return $this->redirectToRoute('book_list');
        }
        
        $classMessage = "Are you sure you want to delete " . $book->getName() . " ?";

        return $this->render('default/delete.html.twig', array(
            'form' => $form->createView(),
            'class_object' => $classMessage
        ));
    }

    
    private function manageImages($form){
        
        $imageName = '';

        $imageFile = $form->get('image')->getData();

        if ($imageFile) {
            $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();

            // Move the file to the directory where images are stored

            $imageFile->move(
                $this->getParameter('images_directory'),
                $newFilename
            );

            // updates the 'image' property to store the image file name
            // instead of its contents
            $imageName = $newFilename;
        }

        return $imageName;

    }

}
