<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }


    public function findAllByAnyField($data){

        $query = $this->createQueryBuilder('b')
            ->where("b.id LIKE '%{$data['any']}%'")
            ->orWhere("b.name LIKE '%{$data['any']}%'")
            ->orWhere("b.description LIKE '%{$data['any']}%'")
            ->orWhere("b.image LIKE '%{$data['any']}%'")
            ->orderBy('b.id', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function nativeSQLQuery(){
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());

        $sql = '
            SELECT b.id, b.name, COUNT(ba.author_id) as author_count
            FROM books as b
            INNER JOIN books_authors as ba ON b.id = ba.book_id
            GROUP BY b.id
            HAVING COUNT(ba.author_id) > 1
        ';

        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('author_count', 'author_count');

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $books = $query->getScalarResult();

        return $books;

    }

    public function DoctrineQuery()
    {
        $qb = $this->createQueryBuilder('b');
        $select = array('b.id', 'b.name', 'count(ba.id) as author_count');

        $query = $qb->select($select)
            ->join('b.authors', 'ba')
            ->groupBy('b.id')
            ->having(
                $qb->expr()->gte(
                    $qb->expr()->count('ba.id'), '3'
                    )
                )
            ->getQuery();

        return $query->getResult();
    }




}
