<?php

namespace App\DataFixtures;
use App\Entity\Book;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BookFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {   $authors = range(1,10);
        $years = range(1900, 2022);

        $booksData = array(
            array("Western Reef-Heron", array('author_1', 'author_8'), 'Voluptatibus qui natus et sint tempora rem commodi. Vero ipsam quod eos sunt assumenda vel voluptas ut. Saepe quos pariatur ea aliquid ad eos. Corrupti voluptas rerum. Optio provident eos laborum quas.'),
            array("Wood Thrush", array('author_2'), 'Eius iste excepturi vero. Eveniet nesciunt aut et sit similique exercitationem iste. Dolores non ullam hic et dicta iure et soluta laborum.'),
            array("Wilson's Storm-Petrel", array('author_3', 'author_8', 'author_5'), 'Placeat molestias facilis provident et libero sequi nulla. Autem sed provident pariatur dolores. Sint doloremque quo corrupti nulla et consequatur amet magni aspernatur. Tempora eveniet consequuntur com'),
            array("Swamp Sparrow", array('author_7', 'author_9'), 'Cumque iure natus nihil architecto non. Aspernatur placeat quibusdam corrupti maxime illo et nihil accusantium. Sequi quisquam officia ipsam expedita voluptas. Quia sequi laudantium repudiandae eligendi aperiam est'),
            array("Spoonbill Sandpiper", array('author_6', 'author_7', 'author_8'), 'Quidem et modi. Placeat est facere nihil. Perspiciatis molestiae fugit molestiae sit error vero. Et minima dignissimos.'),
            array("Roseate Spoonbill", array('author_1'), 'Beatae dolores qui quisquam et porro aut est reprehenderit qui. Et aut temporibus. Iure sunt modi ipsam dolores incidunt dicta.'),
            array("Vesper Sparrow", array('author_2'), 'Impedit non quia ipsam enim recusandae magni non non porro. Blanditiis corrupti praesentium autem. Odio culpa dolor. Eius quasi assumenda sint quia modi veniam. Veniam velit est eum.'),
            array("Louisiana Waterthrush", array('author_3'), 'Sint nihil nisi vel deleniti quam a. Sunt qui ducimus aut doloremque quia sapiente veritatis omnis. Pariatur adipisci fugit ratione maiores optio totam molestiae dolor debitis. Molestias voluptatem tem'),
            array("Curve-billed Thrasher", array('author_5'), 'Autem vero et omnis eos. Dolore hic aliquid qui labore in nihil enim iste. Mollitia sunt corporis optio dolore fugiat blanditiis voluptatem officiis corporis. Voluptatum deserunt qui.'),
            array("Brown-capped Rosy-Finch", array('author_10'), 'Est at ex eos hic sed similique asperiores eveniet. Laborum autem iste excepturi consequuntur omnis. Qui repellat suscipit at sapiente quis. Consequatur dicta quidem non iste cum.'),
            array("Cassin's Sparrow", array('author_4'), 'Quis omnis recusandae id fugit sed unde quas similique id. Culpa hic alias suscipit animi qui. Suscipit omnis et deserunt et cumque reiciendis nam fugiat eum. Quis earum deleniti odit consequatur perspicia'),
            array("Lesser Scaup", array('author_5', 'author_8','author_10'), 'Molestiae voluptas minus voluptatem modi. Ut architecto tempore officiis et qui nihil repellat dicta et. Sit laudantium iusto nihil ipsa illo omnis minima.'),
            array("Cliff Swallow", array('author_3'), 'Dignissimos voluptas qui quod accusamus enim adipisci ut cumque nulla. Quibusdam modi sunt aut. Nobis nobis non mollitia enim. Doloremque consequatur incidunt. Rerum reiciendis sequi tempora. Cum nobis et et plac'),
            array("Mugimaki Flycatcher", array('author_3'), 'Facilis quam fugiat. Asperiores velit ab nulla et quam culpa voluptatem qui. Facilis similique et rerum quidem. Dignissimos omnis quisquam corporis quis. Animi veritatis deleniti ut repellendus dolorem molest'),
        );

        foreach($booksData as $data){
            shuffle($years);
            $entity = new Book();
            $entity->setName($data[0]);
            $entity->setDescription($data[2]);
            $entity->setImage('default.jpeg');
            $entity->setYear($years[0]);
            foreach($data[1] as $author){
                $entity->addAuthor($this->getReference($author));
            }
            $manager->persist($entity);
        }

        $manager->flush();
    }
}
