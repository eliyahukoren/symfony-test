<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $authors = array(
            array('author_1', 'Javier Heller'),
            array('author_2', 'Mr. Percy Turcotte'),
            array('author_3', 'Warren Quigley'),
            array('author_4', 'Marie Pagac'),
            array('author_5', 'Benny Koch'),
            array('author_6', 'Angel Schiller'),
            array('author_7', 'Nellie Collins'),
            array('author_8', 'Anna Dicki'),
            array('author_9', 'Maggie Runte IV'),
            array('author_10', 'Alison Durgan'),
        );

        foreach($authors as $author){
            $entity = new Author();
            $entity->setName($author[1]);
            $this->addReference($author[0], $entity);
            $manager->persist($entity);
        }

        $manager->flush();
    }
}
