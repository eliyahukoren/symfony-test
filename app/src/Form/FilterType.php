<?php

namespace App\Form;

use App\Entity\Author;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', IntegerType::class, array(
                'required' => false,
                'label' => 'Id',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Type any number ...' )
            ))
            ->add('name', TextType::class, array(
                'required' => false,
                'label' => 'Name',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Type any name here ...')
            ))
            ->add('authors', EntityType::class, array(
                'class' => Author::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => true,
                'required' => false,
                'attr' => array('class' => 'form-select')
            ))
            ->add('description', TextType::class, array(
                'required' => false,
                'label' => 'Description',
                'attr' => array('class' =>'form-control', 'placeholder' => 'Type any text here ...')
            ))
            ->add('year', IntegerType::class, array(
                'required' => false,
                'label' => 'Year',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Type any year between 1900 and 2022 ...')
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Search',
                'attr' => array('class' => 'btn btn-primary')
            ));

    }
}
