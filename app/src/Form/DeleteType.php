<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DeleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attrPrimary = array('attr' => array('class' => 'btn btn-primary'));
        $attrDanger = array('attr' => array('class' => 'btn btn-danger'));

        $builder
            ->add( 'delete', SubmitType::class, $attrDanger )
            ->add( 'cancel', SubmitType::class, $attrPrimary );
    }

}
