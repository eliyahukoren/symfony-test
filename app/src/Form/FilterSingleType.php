<?php

namespace App\Form;

use App\Entity\Author;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;


class FilterSingleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('any', TextType::class, array(
                'required' => false,
                'label' => 'Filter',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Filter by any field ...')
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Search',
                'attr' => array('class' => 'btn btn-primary')
            ));

    }
}
