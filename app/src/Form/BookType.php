<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Book;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;   
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name', TextType::class,
                array( 'attr' => array('class' => 'form-control'), 'label' => 'Title')
            )
            ->add(
                'authors', EntityType::class,
                array(
                    'class' => Author::class,
                    'choice_label' => 'name',
                    'expanded' => false,
                    'multiple' => true,
                    'attr' => array('class' => 'form-select')
                )
            )
            ->add(
                'description', TextareaType::class,
                array(
                    'attr' => array(
                        'class' => 'form-control', 
                        'style' => 'height: 100px; overflow:hidden',
                        'maxLength' => "255"
                        )
                )
            )
            ->add(
                'image', FileType::class,
                array(
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Cover',
                    'mapped' =>false,
                    'required' => false,
                    'constraints' => array(
                    new File(
                        array(
                            'maxSize' => '1024k',
                            'mimeTypes' => array('image/jpeg', 'image/png'),
                            'mimeTypesMessage' => 'Please upload a valid JPG, JPEG, PNG files',
                        )
                    ))
            ))
            ->add('year', IntegerType::class,
                        array('attr' => array('class' => 'form-control'))
            )
            ->add(
                'save', SubmitType::class,
                array('attr' => array('class' => 'btn btn-primary'))
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
