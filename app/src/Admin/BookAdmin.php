<?php
// src/Admin/BookAdmin.php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File; 
use App\Entity\Author;
use App\Entity\Book;

final class BookAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof Book
            ? $object->getName()
            : 'Book'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $form): void{
        $form->
            tab('Content')
                ->with('Content', ['class' => 'col-md-9'])
                    ->add( 'name', TextType::class )
                    ->add( 'total_authors', TextType::class, array('required' => false))
                    ->add( 'description', TextareaType::class )
                    ->add( 'authors', EntityType::class,
                                array(
                                    'class' => Author::class,
                                    'choice_label' => 'name',
                                    'expanded' => false,
                                    'multiple' => true,
                                )
                    )
                    ->add( 'year', IntegerType::class, array('required' => false) )
                ->end()
            ->end()
            ->tab('Media')
                ->with('Media', ['class' => 'col-md-9'])
                    ->add( 'image', FileType::class,
                            array('label' => 'Cover',
                                    'mapped' => false,
                                    'required' => false,
                                    'constraints' => array( new File(array('maxSize' => '1024k',
                                                                'mimeTypes' => array('image/jpeg', 'image/png'),
                                                                'mimeTypesMessage' => 'Please upload a valid JPG, JPEG, PNG files',
                                                                        )
                                                                )
                                                            )
                                    )
                    )
                ->end()
            ->end();
    }

    protected function configureListFields(ListMapper $list): void{
        $list->addIdentifier('name')
            ->add('total_authors')
            ->add('description')
            ->add('image')
            ->add('year');
    }

    protected function configureDatagridFilters(DatagridMapper $filter): void{
        $filter->add('name')
            ->add('total_authors')
            ->add('description')
            ->add('image')
            ->add('year');
    }

    protected function configureShowFields(ShowMapper $show): void{
        $show->add('name')
            ->add('total_authors')
            ->add('description')
            ->add('image')
            ->add('year');
    }
}