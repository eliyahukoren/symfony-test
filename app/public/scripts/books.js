
$(document).ready(function () {

    document.querySelectorAll('.fa-floppy-disk').forEach( (item, index) => {
        item.addEventListener('click', event => {
            const book_id = event.target.dataset.id;
            const row = document.querySelectorAll('[data-parent="' + book_id + '"]');

            const data = {
                id: book_id,
                name: row[0].value,
                description: row[1].value,
                year: row[2].value
            }
            request(data);
        })
    })


    function request( data ){
        $.ajax({
            url: '/book/ajax',
            data: JSON.stringify(data),
            contentType: 'application/json',
            type: 'POST',
            dataType: 'json',
            async: true,

            success: function (data, status) {
                alert('Saved');
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('Ajax request failed.');
            }
        });  

    }


});
